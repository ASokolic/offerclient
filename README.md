offers_client
=============

A Symfony project created on May 12, 2018, 1:01 pm. by Andrew Sokolic

#requires *PHP 5.5.9*

## Instructions
run the following comands one you have pulled this repo:
```
$ composer install
```

## Serve 
```
$ php bin/console server:run
```

## Admin Login
Email: admin@admin.com
Password: 123456