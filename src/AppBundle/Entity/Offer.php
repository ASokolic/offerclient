<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 13/05/2018
 * Time: 17:43
 */

namespace AppBundle\Entity;


class Offer
{
    protected $Id;
    protected $Title;
    protected $Description;
    protected $Email;
    protected $Image;
    protected $ImageURL;
    protected $CreateDate;


    public function getTitle(){
        return $this->Title;
    }

    public function setTitle($title){
        $this->Title = $title;
    }

    public function getDescription(){
        return $this->Description;
    }

    public function setDescription($description){
        $this->Description = $description;
    }

    public function getEmail(){
        return $this->Email;
    }

    public function setEmail($email){
        $this->Email = $email;
    }

    public function getImage(){
        return $this->Image;
    }

    public function setImage($image){
        $this->Image = $image;
    }

    public function getImageURL(){
        return $this->ImageURL;
    }

    public function setImageURL($imageUrl){
        $this->ImageURL = $imageUrl;
    }

    public function getCreateDate(){
        return $this->CreateDate;
    }

    public function setCreateDate(\DateTime $createDate = null){
        $this->CreateDate = $createDate;
    }

}