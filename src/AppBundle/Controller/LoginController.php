<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 15/05/2018
 * Time: 00:30
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends Controller
{

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils){

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastEmail = $authenticationUtils->getLastUsername();

        return $this->render('Login/login.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'title'=>'Login',
            'summary'=>'You need to login, to use this site.',
            'error' => $error,
            'lastEmail' => $lastEmail
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(){

    }
}