<?php

namespace AppBundle\Controller;

use AppBundle\ActionHelper\ActionHelper;
use AppBundle\Entity\Offer;
use AppBundle\Service\FileUploader;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class OffersController extends Controller
{
    /**
     * @Route("/", name="list")
     */
    public function listAction()
    {
        $client = new Client(['base_uri' => 'http://localhost:8000/']);

        $offersList = [];
        try {
            $response = $client->get('offers');
            $body = json_decode($response->getBody());
            foreach ($body->Data as $data) {
                $listItem = new \stdClass();
                $listItem->Id = $data->Id;
                $listItem->Title = $data->Title;
                $listItem->Email = $data->Email;
                $listItem->CreateDate = $data->CreateDate;
                $listItem->actions = [
                    new ActionHelper('Edit', '/set/' . $data->Id, ActionHelper::TYPE_EDIT),
                    new ActionHelper('View', '/view/' . $data->Id, ActionHelper::TYPE_VIEW),
                    new ActionHelper('Remove', '/delete/' . $data->Id, ActionHelper::TYPE_REMOVE)
                ];
                $offersList[] = $listItem;
            }
        }catch (ClientException $e){
            $this->addFlash('Error',$e->getCode().': Could not find any Offers');
        }

        return $this->render('Offers/index.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'title'=>'Offers List',
            'summary'=>'Below is a list of offers, browse through and action accordingly.',
            'actions'=>[
                new ActionHelper('Create Offer','/set',ActionHelper::TYPE_ADD)
            ],
            'offers'=>$offersList
        ));
    }

    /**
     * @Route("/set/{id}", defaults={"id"=null}, name="set")
     */
    public function setAction($id = null, Request $request, FileUploader $fileUploader)
    {
        $client = new Client([
            'base_uri' => 'http://localhost:8000/'
        ]);

        $offer = new Offer();
        $title = 'Create New Offer';
        $summary = 'Fill in the form below to create a new Offer.';

        if(!empty($id)){
            $response = $client->get('offers/'.$id);
            $body = json_decode($response->getBody());

            $title = 'Edit Offer #'.$id;
            $summary = 'Use the the form below to edit/change the Offer.';

            $offer->setTitle($body->Data->Title);
            $offer->setDescription($body->Data->Description);
            $offer->setEmail($body->Data->Email);
            $offer->setImageURL($body->Data->ImageURL);
            $offer->setCreateDate(new \DateTime($body->Data->CreateDate));
        }

        $form = $this->createFormBuilder($offer)
            ->add('Title', TextType::class, array('label'=>'Title','attr' => array('class'=>'form-control')))
            ->add('Email', TextType::class,array('label'=>'Email Address','attr' => array('class'=>'form-control')))
            ->add('Description', TextareaType::class,array('label'=>'Offer Description','attr' => array('class'=>'form-control')))
            ->add('Image', FileType::class,array('label'=>'Upload Image','required'=>false,'attr' => array('label'=>'Upload Image','class'=>'form-control')))
            ->add('save', SubmitType::class,array('label'=>'Save Offer','attr' => array('class'=>'btn btn-primary float-right')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $image */
            $image = $offer->getImage();
            if(!empty($image)) {
                $imageName = $fileUploader->upload($image);
                $offer->setImageURL($imageName);
            }

            try {

                $offer = [
                    'Title'=>       $offer->getTitle(),
                    'Email'=>       $offer->getEmail(),
                    'Description'=> $offer->getDescription(),
                    'ImageURL'=>    $offer->getImageURL()
                ];

                if(!empty($id)) {
                    $offer['Id'] = $id;
                    $response = $client->put('/offer', [
                        'headers' => ['content-type' => 'application/json'],
                        'body' => json_encode($offer)
                    ]);
                }else {
                    $response = $client->post('/offer', [
                        'headers' => ['content-type' => 'application/json'],
                        'body' => json_encode($offer)
                    ]);
                }

                if($response->getStatusCode() == 200){
                    $this->addFlash(
                        'Success',
                        "Offer has been Set!"
                    );
                    return $this->redirectToRoute('list');
                }

            }catch (ServerException $e){
                $this->addFlash(
                    'SetError',
                    "OOPS! Something went wrong (".$e->getCode().")"
                );
            }
        }

        return $this->render('Offers/set.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'title'=>$title,
            'summary'=>$summary,
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route("/view/{id}", name="view")
     */
    public function viewAction($id)
    {

        $client = new Client(['base_uri' => 'http://localhost:8000/']);

        $response = $client->get('offers/'.$id);
        $body = json_decode($response->getBody());

        return $this->render('Offers/view.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'title'=>'Offer #'.$id,
            'actions'=>[
                new ActionHelper('Edit','/set/'.$body->Data->Id,ActionHelper::TYPE_EDIT),
                new ActionHelper('Remove','/delete/'.$body->Data->Id,ActionHelper::TYPE_REMOVE)
            ],
            'offer'=>$body->Data
        ));
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction($id){
        $client = new Client(['base_uri' => 'http://localhost:8000/']);

        $response = $client->delete('offers/'.$id);
        $body = json_decode($response->getBody());

        if($body->Status == 200){
            $this->addFlash(
                'Success',
                "Offer has been Deleted!"
            );
        }else{
            $this->addFlash(
                'Error',
                '['.$body->Error .']: '. $body->Message
            );
        }

        return $this->redirectToRoute('list');
    }

}
