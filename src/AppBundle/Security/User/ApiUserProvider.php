<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 15/05/2018
 * Time: 00:05
 */

namespace AppBundle\Security\User;

use AppBundle\Security\User\ApiUser;
use GuzzleHttp\Client;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiUserProvider implements UserProviderInterface
{
    /**
     * @param string $email
     * @return \AppBundle\Security\User\ApiUser
     */
    public function loadUserByUsername($email)
    {

        $client = new Client(['base_uri' => 'http://localhost:8000/']);
        $response = $client->post('user', [
            'headers' => ['content-type' => 'application/json'],
            'body' => json_encode(['Email'=>$email])
            ]);
        $body = json_decode($response->getBody());



        // TODO: add the password validation

        if ($body->Status == 200) {
            return new ApiUser($body->User->Username, $body->User->Email, $body->User->Password, null, ['ROLE_USER']);
        }

        throw new UsernameNotFoundException(
            sprintf('Email "%s" does not exist.', $email)
        );
    }

    /**
     * @param UserInterface $user
     * @return \AppBundle\Security\User\ApiUser
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof ApiUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getEmail());
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return ApiUser::class === $class;
    }
}