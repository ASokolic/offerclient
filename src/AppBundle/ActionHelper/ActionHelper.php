<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 13/05/2018
 * Time: 18:55
 */

namespace AppBundle\ActionHelper;


class ActionHelper
{
    const TYPE_REMOVE   = 'remove';
    const TYPE_ADD      = 'add';
    const TYPE_EDIT     = 'edit';
    const TYPE_SUBMIT   = 'submit';
    const TYPE_DEFAULT  = 'default';
    const TYPE_VIEW     = 'view';

    public $name;
    public $url;
    public $type;

    public function __construct($name = 'Button', $url = '/', $type = self::TYPE_DEFAULT)
    {
        $this->type = $type;
        $this->name = $name;
        $this->url = $url;
    }
}